import { Activity } from "../models/Activity";

const initialActivities = [
    {
        title: 'Hiking in the Mountains',
        description: 'A challenging yet rewarding outdoor activity that involves walking in natural environments, typically on hiking trails in the mountains.',
        category: 'Physical Health',
        duration: 240,
        difficultyLevel: 'Intermediate',
        content: 'Start early to avoid the afternoon sun and ensure you have the right gear. Make sure to stay hydrated and follow marked trails.',
    },
    {
        title: 'Introduction to Programming',
        description: 'A beginner-friendly guide to the basics of programming, covering fundamental concepts and languages.',
        category: 'Productivity',
        duration: 60,
        difficultyLevel: 'Beginner',
        content: 'This session will cover variables, control structures, data types, and basic algorithmic thinking. Perfect for those new to programming.',
    },
    {
        title: 'Yoga for Beginners',
        description: 'A gentle introduction to yoga, focusing on basic postures, breathing techniques, and relaxation methods.',
        category: 'Relaxation',
        duration: 45,
        difficultyLevel: 'Beginner',
        content: 'No previous experience required. Please have a yoga mat ready and wear comfortable clothing that allows for a range of movement.',
    }
];

export async function insertInitialActivities() {
    try {
        if (await Activity.countDocuments() > 0) {
            console.log('Initial activities were already added to the database, Skipping...');
            return;
        }
        await Activity.insertMany(initialActivities);
        console.log('Initial activities have been successfully added to the database.');
    } catch (error) {
        console.error('Error inserting initial activities:', error);
    }
}