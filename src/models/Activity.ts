import mongoose, { Document } from 'mongoose';

export enum ActivityCategory {
    Relaxation = "Relaxation",
    SelfEsteem = "Self-Esteem",
    Productivity = "Productivity",
    PhysicalHealth = "Physical Health",
    SocialConnection = "Social Connection"
}


export interface IActivity extends Document {
    title: string;
    description: string;
    category: ActivityCategory;
    duration: number;
    difficultyLevel: string;
    content: string;
    creationTimestamp: Date;
}

const activitySchema = new mongoose.Schema({
    title: { type: String, required: true },
    description: { type: String, required: true },
    category: { type: String, required: true, enum: Object.values(ActivityCategory) },
    duration: { type: Number, required: true },
    difficultyLevel: { type: String, required: true },
    content: { type: String, required: true },
    creationTimestamp: { type: Date, default: Date.now },
});

export const Activity = mongoose.model<IActivity>('Activity', activitySchema);

