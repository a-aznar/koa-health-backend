import mongoose, { Document } from 'mongoose';

export interface ICompletedActivity extends Document {
    user: mongoose.Schema.Types.ObjectId;
    activity: mongoose.Schema.Types.ObjectId;
    completionDate: Date;
}

const completedActivitySchema = new mongoose.Schema<ICompletedActivity>({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    activity: { type: mongoose.Schema.Types.ObjectId, ref: 'Activity', required: true },
    completionDate: { type: Date, default: Date.now },
});

export const CompletedActivity = mongoose.model<ICompletedActivity>('CompletedActivity', completedActivitySchema);
