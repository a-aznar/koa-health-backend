import mongoose, { Document } from 'mongoose';
import bcrypt from 'bcrypt';

import { ICompletedActivity } from './CompletedActivity';

export interface IUser extends Document {
    username: string;
    email: string;
    password: string;
    completedActivities: ICompletedActivity[]; // Array of CompletedActivity documents (optional)
    comparePassword: (password: string) => Promise<boolean>;
}

const userSchema = new mongoose.Schema({
    username: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
});

const HASH_SALT_ROUNDS = 12;

userSchema.pre<IUser>('save', async function (next) {
    if (!this.isModified('password')) {
        return next();
    }

    this.password = await bcrypt.hash(this.password, HASH_SALT_ROUNDS);

    next();
});

userSchema.methods.comparePassword = async function (candidatePassword: string): Promise<boolean> {
    return bcrypt.compare(candidatePassword, this.password);
};

export const User = mongoose.model<IUser>('User', userSchema);
