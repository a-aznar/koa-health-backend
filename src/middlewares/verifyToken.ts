import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from 'express';

import { config } from '../config';

export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
    const token = req.headers['authorization'];
    if (!token) {
        return res.status(403).json({ message: 'A token is required for authentication' });
    }

    try {
        const decoded = jwt.verify(token, config.jwtSecret) as { id: string };
        req.user = { id: decoded.id };
    } catch (error) {
        return res.status(401).json({ message: 'Invalid Token' });
    }

    return next();
};
