import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { verifyToken } from '../verifyToken';

jest.mock('jsonwebtoken', () => ({
    verify: jest.fn(),
}));

jest.mock('../../config', () => ({
    config: {
        jwtSecret: 'yourSecret',
    },
}));

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mockRequest = (headers: Record<string, any>): Partial<Request> => ({
    headers,
});

const mockResponse = (): Partial<Response> => {
    const res: Partial<Response> = {};
    res.status = jest.fn().mockReturnValue(res);
    res.json = jest.fn().mockReturnValue(res);
    return res;
};

const mockNext = jest.fn();

describe('verifyToken Middleware', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should return 403 if no token is provided', () => {
        const req = mockRequest({});
        const res = mockResponse();

        verifyToken(req as Request, res as Response, mockNext as jest.Mock);

        expect(res.status).toHaveBeenCalledWith(403);
        expect(res.json).toHaveBeenCalledWith({ message: 'A token is required for authentication' });
    });

    it('should return 401 if token is invalid', () => {
        jwt.verify = jest.fn().mockImplementation(() => {
            throw new Error('Invalid token');
        });

        const req = mockRequest({ authorization: 'Bearer invalidToken' });
        const res = mockResponse();

        verifyToken(req as Request, res as Response, mockNext as jest.Mock);

        expect(res.status).toHaveBeenCalledWith(401);
        expect(res.json).toHaveBeenCalledWith({ message: 'Invalid Token' });
    });

    it('should call next if token is valid', () => {
        jwt.verify = jest.fn().mockReturnValue({ id: 'userId' });

        const req: Partial<Request> = mockRequest({ authorization: 'Bearer validToken' });
        const res = mockResponse();

        verifyToken(req as Request, res as Response, mockNext);

        expect(mockNext).toHaveBeenCalled();
    });
});
