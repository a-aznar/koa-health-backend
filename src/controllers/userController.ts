import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import { User } from '../models/User';
import { config } from '../config';
import { CompletedActivity, ICompletedActivity } from '../models/CompletedActivity';
import { Activity } from '../models/Activity';

export const register = async (req: Request, res: Response) => {
    try {
        const { username, email, password } = req.body;

        const existingUser = await User.findOne({ $or: [{ username }, { email }] });

        if (existingUser) {
            return res.status(400).json({ message: 'Username or email already exists' });
        }

        // Create user without manual hashing; Mongoose pre-save hook will handle it
        const user = new User({ username, email, password });

        await user.save();

        res.status(201).json({ message: 'User created successfully' });
    } catch (error) {
        res.status(500).json({ message: 'Error registering user', error });
    }
};


export const login = async (req: Request, res: Response) => {
    try {
        const { email, password } = req.body;
        const user = await User.findOne({ email });

        if (!user) {
            return res.status(401).json({ message: 'Invalid email or password' });
        }

        // Compare submitted password with the hashed password in the database
        const isMatch = await bcrypt.compare(password, user.password);

        if (!isMatch) {
            return res.status(401).json({ message: 'Invalid email or password' });
        }

        // User authenticated, create JWT
        const token = jwt.sign({ id: user._id }, config.jwtSecret, { expiresIn: '1h' });

        // For even better security, it could be used: httpOnly cookie to store the token
        // res.cookie('token', token, { httpOnly: true, secure: true }); // Secure: true, only on HTTPS
        res.json({ message: 'Login successful', token });
    } catch (error) {
        res.status(500).json({ message: 'Error logging in', error });
    }
};

export const markActivityComplete = async (req: Request, res: Response) => {
    try {
        const { activityId } = req.body;
        const userId = req?.user?.id;

        if (!userId) {
            return res.status(403).json({ message: 'User not authenticated' });
        }

        if (!activityId) {
            return res.status(400).json({ message: 'Activity ID is required' });
        }

        const user = await User.findById(userId);

        if (!user) {
            return res.status(404).json({ message: 'User not found' });
        }

        const activity = await Activity.findById(activityId);

        if (!activity) {
            return res.status(404).json({ message: 'Activity not found' });
        }

        const completedActivity = new CompletedActivity({ user: userId, activity: activityId });
        await completedActivity.save();

        res.status(200).json({ message: 'Activity marked as completed successfully' });
    } catch (error) {
        res.status(500).json({ message: 'Error marking activity as completed', error });
    }
};

export const listCompletedActivities = async (req: Request, res: Response): Promise<void> => {
    try {
        const userId = req.user?.id;

        if (!userId) {
            res.status(403).json({ message: 'User not authenticated' });
            return;
        }

        const completedActivities = await CompletedActivity.find({ user: userId })
            .populate('activity')
            .exec();

        // get all activities from ref ids
        const activitiesDetails = completedActivities.map((activity: ICompletedActivity) => activity.activity);

        res.status(200).json(activitiesDetails);
    } catch (error) {
        res.status(500).json({ message: 'Error retrieving completed activities', error });
    }
};
