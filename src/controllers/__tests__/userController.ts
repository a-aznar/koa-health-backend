import { Request, Response } from 'express';
import { listCompletedActivities } from '../userController';
import { CompletedActivity } from '../../models/CompletedActivity';

jest.mock('../../models/CompletedActivity', () => ({
    find: jest.fn().mockReturnThis(),
    populate: jest.fn().mockReturnThis(),
    exec: jest.fn(),
}));

describe('listCompletedActivities', () => {
    let mockReq: Partial<Request>;
    let mockRes: Partial<Response>;
    let mockJson: jest.Mock;
    let mockStatus: jest.Mock;

    beforeEach(() => {
        mockJson = jest.fn();
        mockStatus = jest.fn().mockReturnThis();
        mockReq = {
            user: { id: 'testUserId' }
        };
        mockRes = {
            json: mockJson,
            status: mockStatus,
        };
    });

    it('should require user authentication', async () => {
        mockReq.user = undefined;

        await listCompletedActivities(mockReq as Request, mockRes as Response);

        expect(mockStatus).toHaveBeenCalledWith(403);
        expect(mockJson).toHaveBeenCalledWith({ message: 'User not authenticated' });
    });

    it('should fetch and return all completed activities for the authenticated user', async () => {
        const completedActivities = [
            { activity: { name: 'Jogging', description: 'Morning jog in the park' } },
            { activity: { name: 'Swimming', description: 'Swimming in the local pool' } },
        ];
        (CompletedActivity.find as jest.Mock).mockResolvedValue(completedActivities);

        await listCompletedActivities(mockReq as Request, mockRes as Response);

        expect(CompletedActivity.find).toHaveBeenCalledWith({ user: 'testUserId' });
        expect(CompletedActivity.populate).toHaveBeenCalledWith('activity');
        expect(CompletedActivity.find).toHaveBeenCalled();
        expect(mockStatus).toHaveBeenCalledWith(200);
        expect(mockJson).toHaveBeenCalledWith(completedActivities.map(a => a.activity));
    });

    it('should handle errors while fetching completed activities', async () => {
        const errorMessage = 'Error retrieving completed activities';
        (CompletedActivity.find as jest.Mock).mockRejectedValue(new Error(errorMessage));

        await listCompletedActivities(mockReq as Request, mockRes as Response);

        expect(mockStatus).toHaveBeenCalledWith(500);
        expect(mockJson).toHaveBeenCalledWith({ message: 'Error retrieving completed activities', error: expect.any(Error) });
    });
});
