import { Request, Response } from 'express';
import { listActivities } from '../activityController';
import { Activity } from '../../models/Activity';

jest.mock('../../models/Activity', () => ({
    find: jest.fn(),
}));

describe('listActivities', () => {
    let mockReq: Partial<Request>;
    let mockRes: Partial<Response>;
    let mockSend: jest.Mock;
    let mockStatus: jest.Mock;
    let mockJson: jest.Mock;

    beforeEach(() => {
        mockSend = jest.fn();
        mockStatus = jest.fn().mockReturnThis();
        mockJson = jest.fn();
        mockReq = {};
        mockRes = {
            send: mockSend,
            status: mockStatus,
            json: mockJson,
        };
    });

    it('should fetch and return all activities successfully', async () => {
        const activities = [{ name: 'Hiking' }, { name: 'Swimming' }];
        (Activity.find as jest.Mock).mockResolvedValue(activities);

        await listActivities(mockReq as Request, mockRes as Response);

        expect(Activity.find).toHaveBeenCalled();
        expect(mockJson).toHaveBeenCalledWith(activities);
    });

    it('should return a 500 status code on error', async () => {
        const errorMessage = 'Error fetching activities';
        (Activity.find as jest.Mock).mockRejectedValue(new Error(errorMessage));

        await listActivities(mockReq as Request, mockRes as Response);

        expect(Activity.find).toHaveBeenCalled();
        expect(mockStatus).toHaveBeenCalledWith(500);
        expect(mockJson).toHaveBeenCalledWith({ message: 'Error fetching activities', error: expect.any(Error) });
    });
});
