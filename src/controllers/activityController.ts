import { Request, Response } from 'express';
import { Activity } from '../models/Activity';

export const listActivities = async (req: Request, res: Response) => {
    try {
        const activities = await Activity.find();
        res.json(activities);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching activities', error });
    }
};

// Additional CRUD operations for a possible future back-office system or third-party integrations

export const createActivity = async (req: Request, res: Response) => {
    try {
        const activity = new Activity(req.body);
        await activity.save();
        res.status(201).json({ message: 'Activity created successfully', activity });
    } catch (error) {
        res.status(500).json({ message: 'Error creating activity', error });
    }
};

export const getActivity = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const activity = await Activity.findById(id);
        if (!activity) {
            return res.status(404).json({ message: 'Activity not found' });
        }
        res.json(activity);
    } catch (error) {
        res.status(500).json({ message: 'Error fetching activity', error });
    }
};

export const updateActivity = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const updates = req.body; // The update data will be in the request body
        const activity = await Activity.findByIdAndUpdate(id, updates, { new: true });
        if (!activity) {
            return res.status(404).json({ message: 'Activity not found' });
        }
        res.json({ message: 'Activity updated successfully', activity });
    } catch (error) {
        res.status(500).json({ message: 'Error updating activity', error });
    }
};

export const deleteActivity = async (req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const activity = await Activity.findByIdAndDelete(id);
        if (!activity) {
            return res.status(404).json({ message: 'Activity not found' });
        }
        res.json({ message: 'Activity deleted successfully' });
    } catch (error) {
        res.status(500).json({ message: 'Error deleting activity', error });
    }
};
