import express from 'express';
import swaggerUi from 'swagger-ui-express';
import cors from 'cors';
import mongoose from 'mongoose';
import YAML from 'yaml';
import { readFileSync } from 'fs';
import { join } from 'path';

import { userRouter } from './routes/userRoutes';
import { activityRouter } from './routes/activityRoutes';
import { config } from './config';
import { insertInitialActivities } from './data/Activities';

const app = express();

const openApiSpecFilePath = join(__dirname, '../spec/koa-health.yaml');
const openApiSpecFile = readFileSync(openApiSpecFilePath, 'utf8')
const swaggerDocument = YAML.parse(openApiSpecFile)
// Swagger api docs
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const corsOptions = {
    origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

const MONGO_URI = config.mongoUri;

if (!MONGO_URI) {
    throw new Error('MONGO_URI is not defined');
}

mongoose.connect(MONGO_URI)
    .then(() => console.log('Connected to MongoDB'))
    .catch((err) => console.log('Failed to connect to MongoDB', err));

insertInitialActivities();

app.use(express.json());

// Hello world route
app.get('/', (_req, res) => {
    res.json({ message: 'Hello World!' });
});

app.use('/api/users', userRouter);
app.use('/api/activities', activityRouter);

const PORT = config.appPort;
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
