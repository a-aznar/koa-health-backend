import dotenv from 'dotenv';

dotenv.config();

if (!process.env.APP_PORT) {
    throw new Error('APP_PORT is not defined');
}

if (!process.env.JWT_SECRET) {
    throw new Error('JWT_SECRET is not defined');
}

export const config = {
    appPort: process.env.APP_PORT,
    jwtSecret: process.env.JWT_SECRET,
    mongoUri: process.env.MONGO_URI,
};
