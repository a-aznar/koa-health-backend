import express from 'express';
import * as userController from '../controllers/userController';
import { verifyToken } from '../middlewares/verifyToken';

export const userRouter = express.Router();

userRouter.post('/register', userController.register);
userRouter.post('/login', userController.login);

userRouter.post('/markActivityComplete', verifyToken, userController.markActivityComplete);
userRouter.get('/activitiesComplete', verifyToken, userController.listCompletedActivities);
