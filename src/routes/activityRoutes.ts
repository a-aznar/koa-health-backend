import express from 'express';
import * as activityController from '../controllers/activityController';
import { verifyToken } from '../middlewares/verifyToken';

export const activityRouter = express.Router();

activityRouter.get('/', verifyToken, activityController.listActivities);

activityRouter.post('/', activityController.createActivity);
activityRouter.get('/:id', activityController.getActivity);
activityRouter.put('/:id', activityController.updateActivity);
activityRouter.delete('/:id', activityController.deleteActivity);

