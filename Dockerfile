# Use an official Node.js 20 runtime as the parent image
FROM node:20

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json (or yarn.lock) to the working directory
COPY package*.json ./

# Install project dependencies
RUN npm install

# If you're using TypeScript, compile it to JavaScript
COPY . .
RUN npm run build

# Your application's default port, change it if your app uses a different port
EXPOSE 3000

# Command to run the compiled app
CMD [ "node", "dist/app.js" ]
