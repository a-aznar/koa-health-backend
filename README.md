# README for Mental Well-being Activities Backend

## Overview

This repository contains the Node.js backend for a software platform designed to enhance users' mental well-being through curated activities. These activities are intended to promote various aspects of mental and physical health.

## Features

- **Swagger docs**: Service can be used from Swagger API docs.
- **User Authentication**: Secure registration and login system.
- **Activities Exploration**: Users can list all available activities.
- **Activity Tracking**: Users can mark activities as completed and list their completed activities.

## Getting Started

### Prerequisites

- Node.js
- NPM
- Docker

### Installation

1. Clone the repository:

```bash
git clone [repository_url]
```

2. Run the services with docker compose:

```bash
docker-compose up --build
```

## API Documentation

The backend provides the following RESTful endpoints grouped by functionality:

### Admin

- `POST /activities`: Create a new activity.
- `GET /activities/{id}`: Get a specific activity by its ID.
- `PUT /activities/{id}`: Update an existing activity.
- `DELETE /activities/{id}`: Delete an activity.

### User

- `POST /users/register`: Register a new user.
- `POST /users/login`: Login a user.
- `POST /users/markActivityComplete`: Mark activity as complete.
- `GET /users/activitiesComplete`: List all completed activities.

### Activities

- `GET /activities`: List all activities.

## Database

Uses MongoDB database. Connection handled with mongoose. Some initial data is generated on start for testing purposes.

## Authentication

Implements JWT-based authentication to secure API endpoints, ensuring that only authenticated users can perform actions related to activity data.
Token can be obtained by registering and the login.

## Testing

Run the automated tests using:

```bash
npm run test
```

## Linting

Lint your code to identify and fix potential issues:

```bash
npm run lint
```

## Possible improvements

- Additional testing: integration tests, mock DB, E2E, etc
- Admin role for back-office/third-party endpoints
- MongoDB admin panel available and DB authentication
- Additional error handling for all flows and edge cases
  